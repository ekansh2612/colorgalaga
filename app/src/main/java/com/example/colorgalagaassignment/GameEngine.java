package com.example.colorgalagaassignment;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameEngine extends SurfaceView implements Runnable {




        Thread gameThread = null;
        SurfaceHolder ourHolder;

        // A boolean which we will set and unset
        // when the game is running- or not.
        volatile boolean gameOver;
        private long lastSpawn = 0;
        private int stars = 0;

        // A Canvas and a Paint object
        Canvas canvas;
        Paint paint;

        // This variable tracks the game frame rate
        long fps;
        int score;

        // This is used to help calculate the fps
        private long timeThisFrame;
        private long startFrameTime;
        private int lives;

        // Declare an object of type Bitmap
        Bitmap img;
        Ship ship;

        // Bob starts off not moving
        boolean isMoving = false;

        // He can walk at 150 pixels per second
        float walkSpeedPerSecond = 150;
        private Rectangle screenRectangle;

        // He starts 10 pixels from the left
        float bobXPosition = 10;
        long startTime;

        // When the we initialize (call new()) on gameView
        // This special constructor method runs
        public GameEngine(Context context) {
            // The next line of code asks the
            // SurfaceView class to set up our object.
            // How kind.
            super(context);

            // Initialize ourHolder and paint objects
            ourHolder = getHolder();
            paint = new Paint();

            // Load Bob from his .png file
            img = BitmapFactory.decodeResource(this.getResources(), R.drawable.ship);
            ship = new Ship(img, 100,200);
            MovingObject.objects.add(ship);
            startTime = System.currentTimeMillis();
            screenRectangle = new Rectangle(0, 0, 640, 480);
            gameOver = true;
            lives = 3;
        }

        @Override
        public void run() {
            while (!gameOver) {

                // Capture the current time in milliseconds in startFrameTime
                startFrameTime = System.currentTimeMillis();

                // Update the frame
                update();

                // Draw the frame
                draw();

                // Calculate the fps this frame
                // We can then use the result to
                // time animations and more.
                timeThisFrame = System.currentTimeMillis() - startFrameTime;
                if (timeThisFrame > 0) {
                    fps = 1000 / timeThisFrame;
                }

            }

        }

        // Everything that needs to be updated goes in here
        // In later projects we will have dozens (arrays) of objects.
        // We will also do other things like collision detection.
        public void update() {
            long currentTime = System.currentTimeMillis();
            if((currentTime - startFrameTime) % 1000 == 0 && stars < 30)
                MovingObject.objects.add(new Star());
            // Adds new enemy on screen.
            if((currentTime - startFrameTime) >= 3000) {
                MovingObject.objects.add(new Enemy());
                lastSpawn = currentTime;
            }

            if(isMoving) {
                bobXPosition = bobXPosition + (walkSpeedPerSecond / fps);
            }

        }

        private void GamePaint(Canvas buffer) {
            for (int i = MovingObject.objects.size() - 1; i >= 0; i--)
            {
                MovingObject movingObject = (MovingObject)MovingObject.objects.get(i);

                movingObject.Update();
                movingObject.Draw(buffer);
                // Object out of screen, remove it
                if (!screenRectangle.IntersectsWith(movingObject.GetArea()) || movingObject.status == MovingObject.Status.Exploded)
                {
                    if (movingObject instanceof Star) stars--;
                    MovingObject.objects.remove(movingObject);
                }
                // Check collisions.
                else
                {
                    for (int j = i - 1; j >= 0; j--)
                    {
                        MovingObject collider = (MovingObject)MovingObject.objects.get(j);
                        if (!(collider instanceof Star || movingObject instanceof Star) &&
                                collider.GetArea().IntersectsWith(movingObject.GetArea()) &&
                                !(collider instanceof Enemy && movingObject instanceof Enemy) &&
                                !(collider instanceof Ship && movingObject instanceof Projectile) &&
                                !(collider instanceof Projectile && movingObject instanceof Ship) &&
                                collider.status == MovingObject.Status.Normal &&
                                movingObject.status == MovingObject.Status.Normal)
                        {
                            collider.Explode();
                            movingObject.Explode();

                            if (collider instanceof Enemy || movingObject instanceof Enemy) score += 50;
                        }
                    }
                }
            }

            // If ship was exploded we lost.
            if (ship.status == MovingObject.Status.Exploded && lives == 0)
            {
                gameOver = true;
                Paint paint = new Paint();
                paint.setColor(Color.rgb(0, 255, 255));
                paint.setTextSize(45);
                canvas.drawText("GAME OVER!", 320, 240, paint);
            } else
                ship.status = MovingObject.Status.Normal; // Reset the state of ship.
        }

        // Draw the newly updated scene
        public void draw() {

            // Make sure our drawing surface is valid or we crash
            if (ourHolder.getSurface().isValid()) {
                // Lock the canvas ready to draw
                canvas = ourHolder.lockCanvas();

                // Draw the background color.
                canvas.drawColor(Color.argb(255,  0, 0, 0));

                // Choose the brush color for drawing
                paint.setColor(Color.argb(255,  249, 129, 0));

                // Make the text a bit bigger
                paint.setTextSize(45);

                // Display the current score on the screen.
                canvas.drawText("Score: " + score, 20, 40, paint);
                GamePaint(canvas);

                // Draw everything to the screen
                ourHolder.unlockCanvasAndPost(canvas);
            }

        }

        // If SimpleGameEngine Activity is paused/stopped
        // shutdown our thread.
        public void pause() {
            gameOver = false;
            try {
                gameThread.join();
            } catch (InterruptedException e) {
                Log.e("Error:", "joining thread");
            }

        }

        // If SimpleGameEngine Activity is started then
        // start our thread.
        public void resume() {
            gameOver = true;
            gameThread = new Thread(this);
            gameThread.start();
        }

        // The SurfaceView class implements onTouchListener
        // So we can override this method and detect screen touches.
        @Override
        public boolean onTouchEvent(MotionEvent motionEvent) {

            switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    isMoving = true;
                    break;

                case MotionEvent.ACTION_UP:
                    if(isMoving) {
                        ship.Update(motionEvent);
                        isMoving = false;
                    }

                    break;
            }
            return true;
        }

    }



