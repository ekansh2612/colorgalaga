package com.example.colorgalagaassignment;

import android.graphics.*;
import java.util.*;

public class Star extends MovingObject {
    int light;

    static Random random = null;

    public Star() {
        super(0, -1, 0, 0, 0, 0);
        if (random == null) random = new Random();
        x = random.nextInt(320);
        speedY = random.nextInt(3);
        light = random.nextInt() & 0xF7 + 128;
    }

    public void Draw(Canvas buffer) {
        Paint paint = new Paint();
        paint.setColor(Color.rgb(light, light, light));
        buffer.drawPoint(x, y, paint);
    }
}

