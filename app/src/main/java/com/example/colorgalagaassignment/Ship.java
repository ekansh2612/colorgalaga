package com.example.colorgalagaassignment;

import android.graphics.*;
import android.view.MotionEvent;

class Ship extends MovingObject {
    private Bitmap img;

    public Ship(Bitmap img, float x, float y) {
        super(x, y, 64, 64, 5, 0);
        this.img = img;
    }

    public void Update(MotionEvent motionEvent) {
        int X = (int) motionEvent.getX();
        int Y = (int) motionEvent.getY();

        if (Y >= (y - 20) && Y <= (y + 20)) {
            if (X >= (x + 64))
                x += speedX;
            else if(X < x)
                x -= speedX;
            else
                MovingObject.objects.add(new Projectile(x + (width / 2), y - (height / 2), 5, 0));
        }
    }

    public int getX() {
        return (int)x;
    }

    public int getY() {
        return (int)y;
    }

    @Override
    public void Draw(Canvas buffer) {
        Paint paint = new Paint();
        buffer.drawBitmap(img, x, y, paint);
    }
}
