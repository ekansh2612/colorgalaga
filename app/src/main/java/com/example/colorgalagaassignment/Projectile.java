package com.example.colorgalagaassignment;

import android.graphics.*;

public class Projectile extends MovingObject {
    public Projectile(float x, float y, float speedX, float speedY) {
        super(x, y, 5, 5, speedX, speedY);
    }

    public void Draw(Canvas buffer) {
        Paint paint = new Paint();
        paint.setColor(Color.rgb(255, 255, 0));
        paint.setStyle(Paint.Style.FILL);
        buffer.drawCircle(x, y, 5, paint);
    }
}
