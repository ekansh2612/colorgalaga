package com.example.colorgalagaassignment;

import android.graphics.Canvas;
import java.util.ArrayList;


public abstract class MovingObject {
    public static ArrayList objects = new ArrayList();

    public enum Status
    {
        Normal,
        Exploding,
        Exploded
    }

    public float x, y, speedX, speedY;
    public int width, height;
    public Status status;

    public Rectangle GetArea() {
        return new Rectangle((int)x, (int)y, width, height);
    }

    public MovingObject(float x, float y, int width, int height, float speedX, float speedY) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.speedX = speedX;
        this.speedY = speedY;
        this.status = Status.Normal;
    }

    public void Update()
    {
        x += speedX;
        y += speedY;
    }

    public abstract void Draw(Canvas buffer);

    public void Explode()
    {
        status = Status.Exploded;
    }
}

