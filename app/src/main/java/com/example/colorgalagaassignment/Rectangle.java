package com.example.colorgalagaassignment;

import android.graphics.Point;

public class Rectangle {
    public int x, y;
    public int width, height;

    public Rectangle() {

    }

    public Rectangle(Rectangle rect) {
        this.x = rect.x;
        this.y = rect.y;
        this.width = rect.width;
        this.height = rect.height;
    }


    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public Rectangle(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public boolean Contains(Point point) {
        return inside(point.x, point.y);
    }

    private boolean inside(int x, int y) {
        return width > 0 && height > 0
                && x >= this.x && x < this.x + width
                && y >= this.y && y < this.y + height;
    }

    public boolean IntersectsWith(Rectangle rect) {
        return rect.width > 0 && rect.height > 0 && width > 0 && height > 0
                && rect.x < x + width && rect.x + rect.width > x
                && rect.y < y + height && rect.y + rect.height > y;
    }

}

