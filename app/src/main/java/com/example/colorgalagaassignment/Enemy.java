package com.example.colorgalagaassignment;

import android.graphics.*;
import java.util.Random;


public class Enemy extends MovingObject {
    private enum Type {
        Square,
        Circle
    }

    private Type type;
    private long explosionTime = 0;

    private int originalX;
    private int deathTime = 150;
    static Bitmap img;

    public Enemy() {
        super(0,0,32,32,0,0);
        Random random = new Random(System.currentTimeMillis());
        x = random.nextInt(320 - width);
        originalX = (int)x;
        y = -height;

        if((random.nextInt(2) & 1) == 0)
            type = Type.Square;
        else
            type = Type.Circle;

        if (type == Type.Circle) {
            deathTime = 1000;
        }
        speedY = 0.8F;
    }

    @Override
    public void Update() {
        if (type == Type.Circle) {
            x = originalX + 20 * (float)Math.sin(y / 10);
        }

        if (status == Status.Exploding) {
            if (elapsedTime(explosionTime) > deathTime) {
                status = Status.Exploded;
            }
        }
        super.Update();
    }

    private long elapsedTime(long explosionTime) {
        long Time = System.currentTimeMillis();
        if (explosionTime > Time) return Time + (1000000000 - explosionTime);
        return Time - explosionTime;
    }

    public void Draw(Canvas buffer) {
        Paint paint = new Paint();
        int val = (type == Type.Square) ? 1 : 2;

        switch (val) {
            case 1:
            {
                if (status == Status.Normal) {
                    paint.setColor(Color.rgb(255, 0, 0));
                    paint.setStyle(Paint.Style.FILL);
                    buffer.drawRect(x, y, width, height, paint);
                }
                else {
                    int drift = (int)elapsedTime(explosionTime) / 10;
                    paint.setColor(Color.rgb(255 * (15 - drift) / 15, 0, 0));
                    paint.setStyle(Paint.Style.FILL);
                    int side = 10;

                    buffer.drawRect((int)x - drift, (int)y - drift, (int)(x - drift + side), (int)(y - drift + side), paint);
                    buffer.drawRect((int)x + 10 + drift, (int)y - drift, (int)(x + 10 + drift + side), (int)(y - drift + side), paint);
                    buffer.drawRect((int)x + 10 + drift, (int)y + 10 + drift, (int)(x + 10 + drift + side), (int)(y + 10 + drift + side), paint);
                    buffer.drawRect((int)x - drift, (int)y + 10 + drift, (int)(x - drift + side), (int)(y + 10 + drift + side), paint);
                }
            }
            break;
            case 2:
            {
                if (status == Status.Normal) {
                    paint.setColor(Color.rgb(255, 0, 255));
                    paint.setStyle(Paint.Style.FILL);
                } else {
                    int color = (int)(255 * (100 - (elapsedTime(explosionTime) / 10)) / 100);
                    int radius = (int)(10 *(100 - (elapsedTime(explosionTime) / 10)) / 100);

                    paint.setColor(Color.rgb(color, 0, color));
                    paint.setStyle(Paint.Style.FILL);
                    buffer.drawCircle((int)x + width / 2, (int)y + width / 2, radius, paint);
                }
            }
            break;
            default: break;
        }
    }

    @Override
    public void Explode() {
        if (status == Status.Normal) {
            status = Status.Exploding;
            explosionTime = System.currentTimeMillis();
        }
    }
}

