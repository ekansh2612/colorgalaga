package com.example.colorgalagaassignment;



import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.support.v7.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {

    GameEngine gameEngine;


    // screen size variables
    Display display;
    Point size;


    @Override
    protected  void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        display = getWindowManager().getDefaultDisplay();
        size = new Point();

        display.getSize(size);
        screenWidth = size.x;
        screenHeight = size.y;

        gameEngine = new GameEngine(this, screenWidth, screenHeight);

        setContentView(gameEngine);
    }


    @Override
    protected  void onPause() {
        super.onPause();
        gameEngine.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameEngine.resume();

    }


}


